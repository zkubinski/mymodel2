#include "mymodel2.h"
#include <QDebug>
#include <QVector>
#include <QVariant>
#include <QStringList>
#include <QRegularExpression>

MyModel2::MyModel2(QWidget *parent, QStringList _data, int _row, int _column) : Matrix(_data), myRow(_row), myColumn(_column)
{    
    (void) parent;
//    Matrix << ("1;x;7.5;=;7\n") << ("2;x;7;=;14\n");

    QString strData;
    QStringList field;
    MyDataModel data;

    QRegularExpression regex("[\n|;]");

    for(int i=0; i<Matrix.size(); ++i){
        strData = Matrix.at(i);
        field = strData.split(regex, Qt::SkipEmptyParts);

        data.setLiczba1(field.at(0));
        data.setDzialanie(field.at(1));
        data.setLiczba2(field.at(2));
        data.setZnak(field.at(3));
        data.setWynik(field.at(4));

        myData.push_back(data);
    }

//    for(int i=0; i<myData.size(); ++i){
//        qDebug()<<myData[i].getLiczba1();
//        qDebug()<<myData[i].getDzialanie();
//        qDebug()<<myData[i].getLiczba2();
//        qDebug()<<myData[i].getZnak();
//        qDebug()<<myData[i].getWynik();
//        qDebug()<<"\n";
//    }
}

Qt::ItemFlags MyModel2::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);

    if(index.column()==0 || index.column()==1 || index.column()==2 || index.column()==3 || index.column()==4){
        flags = flags | Qt::ItemIsEditable;
    }
    return flags;
}

int MyModel2::columnCount(const QModelIndex &parent) const
{
    if(parent.isValid()){
        return 0;
    }
    else{
        return myColumn;
    }
}
int MyModel2::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid()){
        return 0;
    }
    else{
        return myRow;
    }
}

QModelIndex MyModel2::index(int row, int column, const QModelIndex &parent) const
{
    QModelIndex idx;

    idx=createIndex(row,column,nullptr);

    if(parent.isValid()){
        return idx;
    }

    return idx;
}

QModelIndex MyModel2::parent(const QModelIndex &index) const
{
    if(!index.isValid()){
        return QModelIndex();
    }
    return QModelIndex();
}

//"setData" - ustawia dane w modelu i zmienia te dane w przypadku gdy zaszły jakieś zmiany
bool MyModel2::setData(const QModelIndex &index, const QVariant &value, int role)
{
    bool result = false;

    beginResetModel();

    if(index.isValid() && role == Qt::EditRole){
        int row = index.row();
        int column = index.column();

        switch(column)
        {
            case 0:
                myData[row].setLiczba1(value.toString());
                qDebug()<<"kolumna ("<<column<<") ="<<myData.at(row).getLiczba1();
                break;

            case 1:
                myData[row].setDzialanie(value.toString());
                qDebug()<<"kolumna ("<<column<<") ="<<myData.at(row).getDzialanie();
                break;

            case 2:
                myData[row].setLiczba2(value.toString());
                qDebug()<<"kolumna ("<<column<<") ="<<myData.at(row).getLiczba2();
                break;

            case 3:
                myData[row].setZnak(value.toString());
                qDebug()<<"kolumna ("<<column<<") ="<<myData.at(row).getZnak();
                break;

            case 4:
                myData[row].setWynik(value.toString());
                qDebug()<<"kolumna ("<<column<<") ="<<myData.at(row).getWynik();
                break;

            default:
                break;
        }

        emit dataChanged(index, index, {Qt::EditRole, Qt::DisplayRole});
     }

    endResetModel();

    return result;
}

//"data" - ustawia dane w widoku
QVariant MyModel2::data(const QModelIndex &index, int role) const
{
    QVariant result = QVariant();

    unsigned int row = static_cast<unsigned int>(index.row());
    unsigned int col = static_cast<unsigned int>(index.column());

    if(!index.isValid()){
        return result;
    }

    switch(role){
        case Qt::DisplayRole:{
            switch (col){
                case 0:
                    result = myData.at(row).getLiczba1();
                break;

                case 1:
                    result = myData.at(row).getDzialanie();
                break;

                case 2:
                    result = myData.at(row).getLiczba2();
                break;

                case 3:
                    result = myData.at(row).getZnak();
                break;

                case 4:
                    result = myData.at(row).getWynik();
                break;
            }
        }
    }

    return result;
}

QVariant MyModel2::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole && orientation==Qt::Horizontal){
        switch (section){
        case 0:
            return QString("liczba");

        case 1:
            return QString("działanie");

        case 2:
            return QString("liczba");

        case 3:
            return QString("równa się");

        case 4:
            return QString("wynik");
        }
    }

    if(role==Qt::DisplayRole && orientation==Qt::Vertical){

        for(int i=0; i<=section; ++i){
            return QString("Lp %1").arg(section+1);
        }

//        switch (section){
//        case 0:
//            return QString("Lp %1").arg(section+1);

//        case 1:
//            return QString("Lp %1").arg(section+1);

//        case 2:
//            return QString("Lp %1").arg(section+1);

//        case 3:
//            return QString("Lp %1").arg(section+1);

//        case 4:
//            return QString("Lp %1").arg(section+1);
//        }
    }

    return QVariant();
}
