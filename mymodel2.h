#ifndef MYMODEL2_H
#define MYMODEL2_H

#include <QAbstractItemModel>
#include <QVector>
#include "mydatamodel.h"

class Dzialania;

class MyModel2 : public QAbstractItemModel
{
    Q_OBJECT

public:
    MyModel2(QWidget *parent = nullptr, QStringList={""}, int _row=0, int _column=0);

    enum{liczba1=0, dzialanie=1, liczba2=2, znak=3, wynik=4};

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    //"setData" - ustawia dane w modelu
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    //"data" - ustawia dane w widoku
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    int	rowCount(const QModelIndex &parent = QModelIndex()) const override;

private:
    QStringList Matrix;
    MyDataModel Data;
    int myRow, myColumn;

    QVector<MyDataModel> myData;
//    std::vector<MyDataModel> myData;

private slots:
};

#endif // MYMODEL2_H
