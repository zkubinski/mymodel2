#ifndef MYDATAMODEL_H
#define MYDATAMODEL_H

#include <QString>

class MyDataModel
{
public:
    MyDataModel();

    void setLiczba1(QString _liczba1);
    void setDzialanie(QString _dzialanie);
    void setLiczba2(QString _liczba2);
    void setZnak(QString _znak);
    void setWynik(QString _wynik);

    QString getLiczba1() const;
    QString getDzialanie() const;
    QString getLiczba2() const;
    QString getZnak() const;
    QString getWynik() const;

private:
    QString liczba1;
    QString dzialanie;
    QString liczba2;
    QString znak;
    QString wynik;
};

#endif // MYDATAMODEL_H
