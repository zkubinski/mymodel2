#include "mainwindow.h"
#include "mymodel2.h"
#include <QDebug>
#include <QStringList>
#include <QItemSelectionModel>
#include "myopencsv.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    resize(600,370);

    mWidget = new QWidget(this);
    mLayout = new QVBoxLayout(mWidget);

    pbLayout = new QHBoxLayout();

    pbOpenFile = new QPushButton();
    pbOpenFile->setText("Otwórz plik CSV");

    pbSaveFile = new QPushButton();
    pbSaveFile->setText("Zapisz do pliku CSV");

    pbQuit = new QPushButton();
    pbQuit->setText("Zamknij");

    QStringList dane;
    dane << ("1;x;7.5;=;7\n") << ("2;x;7;=;14\n");

    absModel = new MyModel2(nullptr, dane, 2, 5);

    myView = new QTableView();
    myView->setModel(absModel);

    QItemSelectionModel *selModel = myView->selectionModel();
    absModel->index(0,0,QModelIndex());

    QItemSelection sel(absModel->index(0,0,QModelIndex()), absModel->index(0,0,QModelIndex()));
    selModel->select(sel,QItemSelectionModel::Select);

    QObject::connect(myView->model(), &QAbstractItemModel::dataChanged, this, &MainWindow::przeladuj);
    QObject::connect(pbOpenFile, &QPushButton::clicked, this, &MainWindow::OpenFileDialog);
    QObject::connect(pbQuit, &QPushButton::clicked, this, &QMainWindow::close);

    mWidget->setLayout(mLayout);

    pbLayout->addWidget(pbOpenFile);
    pbLayout->addWidget(pbSaveFile);
    pbLayout->addWidget(pbQuit);

    mLayout->addWidget(myView);
    mLayout->addLayout(pbLayout);

    setCentralWidget(mWidget);
}

void MainWindow::OpenFileDialog(void)
{
    QString filter = "All Files (*.*) ;; Text File (*.txt) ;; CSV File (*.csv)";
    QString getFileName = QFileDialog::getOpenFileName(this, QString("Open File"), QDir::homePath(), filter);

//    myOpenCSV mycsv;
//    mycsv.myOpenFile(getFileName);

//    qDebug()<<"zliczone kolumny"<< mycsv.myColumns();
//    qDebug()<<"zliczone wiersze"<< mycsv.myRow();
}
