#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTableView>
#include <QAbstractItemModel>
#include <QPushButton>

#include <QString>
#include <QFile>
#include <QFileDialog>


#include <QDebug>
#include <QMessageBox>

#include "mydatamodel.h"
#include "myopencsv.h"
#include <QRegularExpression>

class MyModel2;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    QWidget *mWidget;
    QVBoxLayout *mLayout;
    QHBoxLayout *pbLayout;

    MyModel2 *absModel;
    QTableView *myView;

    QPushButton *pbOpenFile, *pbSaveFile, *pbQuit;

    unsigned int rows=0;
    unsigned int columns=0;
    bool znak=false;

    QStringList field;

signals:

private slots:
    inline void przeladuj(const QModelIndex &topLeft, const QModelIndex &topRight, const QVector<int> &roles)
    {
        Q_UNUSED(topLeft);
        Q_UNUSED(topRight);
        Q_UNUSED(roles);

        qDebug()<<"przeladowano";
    }

    void OpenFileDialog();
};

#endif // MAINWINDOW_H
