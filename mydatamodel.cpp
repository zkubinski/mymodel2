#include "mydatamodel.h"

MyDataModel::MyDataModel():liczba1("L1"),dzialanie("Dz"),liczba2("L2"),znak("Zn"),wynik("Wyn")
{

}

void MyDataModel::setLiczba1(const QString _liczba1)
{
    liczba1=_liczba1;
}

void MyDataModel::setDzialanie(QString _dzialanie)
{
    dzialanie=_dzialanie;
}

void MyDataModel::setLiczba2(QString _liczba2)
{
    liczba2=_liczba2;
}

void MyDataModel::setZnak(QString _znak)
{
    znak=_znak;
}

void MyDataModel::setWynik(QString _wynik)
{
    wynik=_wynik;
}
/* ############################################### */
QString MyDataModel::getLiczba1() const
{
    return liczba1;
}

QString MyDataModel::getDzialanie() const
{
    return dzialanie;
}

QString MyDataModel::getLiczba2() const
{
    return liczba2;
}

QString MyDataModel::getZnak() const
{
    return znak;
}

QString MyDataModel::getWynik() const
{
    return wynik;
}
